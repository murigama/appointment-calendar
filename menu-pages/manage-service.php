<div style="background:#C3D9FF; margin-bottom:10px; padding-left:10px;"><h3><?php _e("Services", "appointzilla"); ?></h3></div>
<!-- manage service form -->	
<div class="bs-docs-example tooltip-demo">
<?php 
global $wpdb;
if(isset($_GET['sid']))
{	
	$sid = $_GET['sid'];
	$table_name = $wpdb->prefix . "ap_services";
	$servicedetails="SELECT * FROM $table_name WHERE `id` ='$sid'";
	$servicedetails = $wpdb->get_row($servicedetails);
	$servicedetails->category_id; 
}
else
{
	$servicedetails = NULL;
}
?>
<form action="" method="post" name="manageservice">
	<table width="100%" class="table" >
	   <tr>
		<th width="18%" scope="row"><?php _e("Name", "appointzilla"); ?></th>
		<td width="5%">&nbsp;</td>
		<td width="77%"><input name="name" type="text" id="name"  value="<?php if($servicedetails) { echo $servicedetails->name; } ?>" class="inputheight"/>&nbsp;<a href="#" rel="tooltip" title="<?php _e("Service Name", "appointzilla"); ?>" ><i class="icon-question-sign"></i></a></td>
	  </tr>
	  <tr>
		<th scope="row"><strong><?php _e("Description", "appointzilla"); ?></strong></th>
		<td>&nbsp;</td>
		<td><textarea name="desc" id="desc"><?php if($servicedetails) { echo $servicedetails->desc; } ?></textarea>&nbsp;<a href="#" rel="tooltip" title="<?php _e("Service Description", "appointzilla"); ?>" ><i class="icon-question-sign"></i></a>
		</td>
	  </tr>
	   <tr>
		<th scope="row"><strong><?php _e("Duration", "appointzilla"); ?></strong></th>
		<td>&nbsp;</td>
		<td><input name="Duration" type="text" id="Duration"  value="<?php if($servicedetails) { echo $servicedetails->duration; } ?>" class="inputheight"/>&nbsp;<a href="#" rel="tooltip" title="<?php _e("Service Duration.<br>Enter Numeric Value.<br>Eg: 5, 10, 15, 30, 60", "appointzilla"); ?>" ><i class="icon-question-sign"></i> </a></td>
	  </tr>
	  
	  <tr>
		<th scope="row"><strong><?php _e("Duration Unit", "appointzilla"); ?></strong></th>
		<td>&nbsp;</td>
		<td><select id="durationunit" name="durationunit">
		<option value="0"><?php _e("Select Duration's Unit", "appointzilla"); ?></option>
		<option value="minute" <?php if($servicedetails) { if($servicedetails->unit == 'minute') echo "selected"; } ?> ><?php _e("Minute(s)", "appointzilla"); ?></option>
		</select>&nbsp;<a href="#" rel="tooltip" title="<?php _e("Duration Unit", "appointzilla"); ?>" ><i class="icon-question-sign"></i></a>
		</td>
	  </tr>
	  <tr>
		<th scope="row"><strong><?php _e("Cost", "appointzilla"); ?></strong></th>
		<td>&nbsp;</td>
		<td><input name="cost" type="text" id="cost" value="<?php if($servicedetails) { echo $servicedetails->cost; } ?>" class="inputheight"/>&nbsp;<a href="#" rel="tooltip" title="<?php _e("Service Cost<br>Enter Numeric Value<br> Eg: 5 , 10, 25, 50, 100, 150", "appointzilla"); ?>" ><i class="icon-question-sign"></i></a>
		</td>
	  </tr>
	  <tr>
		<th scope="row"><strong><?php _e("Availability", "appointzilla"); ?></strong></th>
		<td>&nbsp;</td>
		<td>
		<select id="availability" name="availability">
		<option value="0"><?php _e("Select Service Availability", "appointzilla"); ?></option>
		<option value="yes" <?php if($servicedetails) { if($servicedetails->availability == 'yes') echo "selected"; } ?> ><?php _e("Yes", "appointzilla"); ?></option>
		<option value="no" <?php if($servicedetails) { if($servicedetails->availability == 'no') echo "selected"; } ?> ><?php _e("No", "appointzilla"); ?></option>
		</select>&nbsp;<a href="#" rel="tooltip" title="<?php _e("Service Availability", "appointzilla"); ?>" ><i class="icon-question-sign"></i></a>
		</td>
	  </tr>
	  <tr>
		<th scope="row"><?php _e("Category", "appointzilla"); ?></th>
		<td>&nbsp;</td>
		
		<td><select id="category" name="category">
			  <option value="0"><?php _e("Select Category", "appointzilla"); ?></option>
			  <?php //get all category list					 	
				$table_name = $wpdb->prefix . "ap_service_category";
				$service_category = $wpdb->get_results("select * from $table_name"); 
				foreach($service_category as $gruopname) 
				{	?>
					<option value="<?php echo $gruopname->id; ?>" 
						<?php if($servicedetails) { if($servicedetails->category_id == $gruopname->id) echo "selected";  ?><?php if(isset($_GET['gid']) == $gruopname->id) echo "selected"; } ?> >
						<?php echo $gruopname->name; ?>
					</option>
				<?php } ?>
			</select>&nbsp;<a href="#" rel="tooltip" title="<?php _e("Service Category", "appointzilla"); ?>" ><i class="icon-question-sign"></i></a>
		</td>
	  </tr>
	  <tr>
		<th scope="row">&nbsp;</th>
		<td>&nbsp;</td>
		<td> <?php if(isset($_GET['sid']))	{	?>
			<button id="saveservice" type="submit" class="btn btn-primary" name="updateservice"><i class="icon-pencil icon-white"></i> <?php _e("Update", "appointzilla"); ?></button>
			<?php } else {?>
			<button id="saveservice" type="submit" class="btn btn-primary" name="saveservice"><i class="icon-ok icon-white"></i> <?php _e("Create", "appointzilla"); ?></button>
			<?php } ?>
			<a href="?page=service" class="btn btn-primary"><i class="icon-remove icon-white"></i> <?php _e("Cancel", "appointzilla"); ?></a>
		</td>
	  </tr>
  </table>
</form>
	
<?php //inserting a service
if(isset($_POST['saveservice']))	
{
	$servicename=$_POST['name'];
	$desc=$_POST['desc'];
	$Duration=$_POST['Duration'];
	$durationunit=$_POST['durationunit'];
	$cost=$_POST['cost'];
	$availability=$_POST['availability'];
	$category=$_POST['category'];
	
	$table_name = $wpdb->prefix . "ap_services";
	$insert_service = "INSERT INTO $table_name (
						`name` ,
						`desc` ,
						`duration` ,
						`unit` ,
						`cost` ,
						`availability`,
						`category_id`
						)VALUES ('$servicename', '$desc', '$Duration', '$durationunit', '$cost', '$availability', '$category');";
	if($wpdb->query($insert_service))
	{
		echo "<script>alert('". __('New service successfully created.', 'appointzilla') ."');</script>";
	}
	echo "<script>location.href='?page=service';</script>";
}

 //upadte a service 
if(isset($_POST['updateservice']))	
{
	$sid=$_GET['sid']; 
	$servicename=$_POST['name'];
	$desc=$_POST['desc'];
	$Duration=$_POST['Duration'];
	$durationunit=$_POST['durationunit'];
	$cost=$_POST['cost'];
	$availability=$_POST['availability'];
	$category=$_POST['category'];
	
	$table_name = $wpdb->prefix . "ap_services";
	$update_service ="UPDATE $table_name SET `name` = '$servicename',
						`desc` = '$desc',
						`duration` = '$Duration',
						`unit` = '$durationunit',
						`cost` = '$cost',
						`availability` = '$availability',
						`category_id` = '$category' WHERE `id` ='$sid';";
						
	$wpdb->query($update_service);
	echo "<script>alert('". __('Service successfully updated.', 'appointzilla') ."');</script>";
	echo "<script>location.href='?page=service';</script>";						
}
?>					
	
<style type="text/css">
.error{  color:#FF0000; }
input.inputheight { height:30px; }
</style>
<!--validation js lib-->
<script src="<?php echo plugins_url('/js/jquery.min.js', __FILE__); ?>" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function () {
	
	$('#saveservice').click(function() 
	{
		$('.error').hide();  
		var name = $("input#name").val();  
		if (name == "")
		{  	
			$("#name").after('<span class="error">&nbsp;<br><strong><?php _e("Name cannot be blank.", "appointzilla"); ?></strong></span>');
			return false; 
		}
		else
		{	var name = isNaN(name);
			if(name == false) 
			{ 	
			$("#name").after('<span class="error">&nbsp;<br><strong><?php _e("Invalid name.", "appointzilla"); ?></strong></span>');
			return false; 
			}
		}
		
		var desc = $("textarea#desc").val();  
		if (desc == "")
		{  	$("#desc").after('<span class="error">&nbsp;<br><strong><?php _e('Description  cannot be blank.','appointzilla');?></strong></span>');
			return false; 
		}
		
		var Duration = jQuery("input#Duration").val();  
		if (Duration == "")
		{  	jQuery("#Duration").after('<span class="error">&nbsp;<br><strong><?php _e('Duration cannot be blank.','appointzilla');?></strong></span>');
			return false; 
		}
		else if(Duration != 0)
		{	
			var Duration = isNaN(Duration);
			if(Duration == true) 
			{ 	
				jQuery("#Duration").after('<span class="error">&nbsp;<br><strong><?php _e('Invalid Duration.','appointzilla');?></strong></span>');
				return false; 
			}
			else
			{
				var Duration = jQuery("input#Duration").val(); 
				var testvalue = Duration%5;
				if(testvalue !=0)
				{
					jQuery("#Duration").after('<span class="error">&nbsp;<br><strong><?php _e('Duration will be in multiple of 5, like as: 5, 10, 15, 20, 25.','appointzilla');?></strong></span>');
					return false; 
				}
			}
		}
		else
		{
			jQuery("#Duration").after('<span class="error">&nbsp;<br><strong><?php _e('Duration will be in multiple of 5, like as: 5, 10, 15, 20, 25.','appointzilla');?></strong></span>');
					return false; 
		}
		
		var durationunit = $('#durationunit').val();
		if(durationunit == 0)
		{
			$("#durationunit").after('<span class="error">&nbsp;<br><strong><?php _e("Select Durations Unit.", "appointzilla"); ?></strong></span>');
			return false;
		}
		
		var cost = $("input#cost").val();  
		if (cost == "")
		{  	$("#cost").after('<span class="error">&nbsp;<br><strong><?php _e("Cost cannot be blank.", "appointzilla"); ?></strong></span>');
			return false; 
		}
		else
		{	var cost = isNaN(cost);
			if(cost == true) 
			{ 	
			$("#cost").after('<span class="error">&nbsp;<br><strong><?php _e("Invalid cost.", "appointzilla"); ?></strong></span>');
			return false; 
			}
		}
		
		var availability = $('#availability').val();
		if(availability == 0)
		{
			$("#availability").after('<span class="error">&nbsp;<br><strong><?php _e("Select availability.", "appointzilla"); ?></strong></span>');
			return false;
		}
		
		var category = $('#category').val();
		if(category == 0)
		{
			$("#category").after('<span class="error">&nbsp;<br><strong><?php _e("Select category.", "appointzilla"); ?></strong></span>');
			return false;
		}
	});
});
</script>
</div>