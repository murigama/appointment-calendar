<div class="bs-docs-example tooltip-demo">
<div style="background:#C3D9FF; margin-bottom:10px; padding-left:10px;">
  <h3><?php _e('Manage Notification Settings', 'appointzilla'); ?></h3> 
</div>
<form name="emailsettings" action="?page=notificationsettings" method="post">
  <table width="100%" class="table">
  <tr>
    <th colspan="2" scope="row"><?php _e('Enable', 'appointzilla'); ?></th>
    <td width="3%"><strong>:</strong></td>
    <td width="69%"><input name="enable" type="checkbox" id="enable" <?php if(get_option('emailstatus') == 'on') echo 'checked'; ?> />&nbsp;<a href="#" rel="tooltip" title="<?php _e('ON/OFF Notification', 'appointzilla'); ?>" ><i class="icon-question-sign"></i> </a>
	</td>
    <td width="3%">&nbsp;</td>
    <td width="3%">&nbsp;</td>
    <td width="3%">&nbsp;</td>
  </tr>
  <?php
   $emailtype = get_option('emailtype');
  ?>
  <tr>
    <th colspan="2" scope="row"><?php _e('Email Type', 'appointzilla'); ?></th>
    <td><strong>:</strong></td>
    <td>
      <select name="emailtype" id="emailtype">
        <option value="0"><?php _e('Select Type', 'appointzilla'); ?></option>
        <option value="wpmail" <?php if($emailtype == 'wpmail') echo 'selected';?>><?php _e('WP Mail', 'appointzilla'); ?></option>
        <option value="phpmail" <?php if($emailtype == 'phpmail') echo 'selected';?>><?php _e('PHP Mail', 'appointzilla'); ?></option>
        <option value="smtp" <?php if($emailtype == 'smtp') echo 'selected';?>><?php _e('SMTP Mail', 'appointzilla'); ?></option>
      </select>&nbsp;<a href="#" rel="tooltip" title="<?php _e('Notification Type', 'appointzilla'); ?>" ><i class="icon-question-sign"></i></a>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
	<?php 
		$emaildetails =  get_option('emaildetails');
		if($emaildetails)
		{
			$emaildetails = unserialize($emaildetails);
		}
	?>
<!--wp mail-->
  <tr id="wpmaildetails1" style="display:none;">
    <th colspan="2" scope="row"><?php _e('WP Mail Details', 'appointzilla'); ?></th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr id="wpmaildetails2" style="display:none;">
    <th scope="row">&nbsp;</th>
    <th scope="row"><?php _e('Email', 'appointzilla'); ?></th>
    <td><strong>:</strong></td>
    <td><input name="wpemail" type="text" id="wpemail"  value="<?php if(isset($emaildetails['wpemail'])) { echo $emaildetails['wpemail']; } ?>" />&nbsp;<a href="#" rel="tooltip" title="<?php _e('Admin Email', 'appointzilla'); ?>" ><i class="icon-question-sign"></i></a>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</div>


	<!--php mail-->
  <tr id="phpmaildetails1" style="display:none;">
    <th colspan="2" scope="row"><?php _e('PHPMail Details', 'appointzilla'); ?></th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr id="phpmaildetails2" style="display:none;">
    <th scope="row">&nbsp;</th>
    <th scope="row"><?php _e('Email', 'appointzilla'); ?></th>
    <td><strong>:</strong></td>
    <td><input name="phpemail" type="text" id="phpemail" value="<?php if(isset($emaildetails['phpemail'])) { echo $emaildetails['phpemail']; } ?>" />&nbsp;<a href="#" rel="tooltip" title="<?php _e('Admin Email', 'appointzilla'); ?>" ><i  class="icon-question-sign"></i></a>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>


<!--smtp-->
  <tr id="smtpdetails1" style="display:none;">
    <th colspan="2" scope="row"><?php _e('SMTP Mail Details', 'appointzilla'); ?></th>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr id="smtpdetails2" style="display:none;">
    <th width="9%" scope="row">&nbsp;</th>
    <td width="10%" scope="row"><?php _e('Host Name', 'appointzilla'); ?></td>
    <td><strong>:</strong></td>
    <td><input name="hostname" type="text" id="hostname" class="inputhieght" value="<?php if(isset($emaildetails['hostname'])) { echo $emaildetails['hostname']; } ?>" />&nbsp;<a href="#" rel="tooltip" title="<?php _e('Host Name', 'appointzilla'); ?><br>Like Eg: <br>Gmail = smtp.gmail.com, <br>Yahoo = smtp.yahoo.com" ><i class="icon-question-sign"></i></a>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr id="smtpdetails3" style="display:none;">
    <th scope="row">&nbsp;</th>
    <td scope="row"><?php _e('Port Number', 'appointzilla'); ?></td>
    <td><strong>:</strong></td>
    <td><input name="portno" type="text" id="portno" value="<?php if(isset($emaildetails['portno'])) { echo $emaildetails['portno']; } ?>" />&nbsp;<a href="#" rel="tooltip" title="<?php _e('SMTP Port Number', 'appointzilla'); ?><br>Gmail & Yahoo Port Number = 465" ><i class="icon-question-sign"></i></a>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr id="smtpdetails4" style="display:none;">
    <th scope="row">&nbsp;</th>
    <td scope="row"><?php _e('Email', 'appointzilla'); ?></td>
    <td><strong>:</strong></td>
    <td><input name="smtpemail" type="text" id="smtpemail" value="<?php if(isset($emaildetails['smtpemail'])) { echo $emaildetails['smtpemail']; } ?>" />&nbsp;<a href="#" rel="tooltip" title="<?php _e('Admin SMTP Email', 'appointzilla'); ?>" ><i class="icon-question-sign"></i></a>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr id="smtpdetails5" style="display:none;">
    <th scope="row">&nbsp;</th>
    <td scope="row"><?php _e('Password', 'appointzilla'); ?></td>
    <td><strong>:</strong></td>
    <td><input name="password" type="password" id="password" value="<?php if(isset($emaildetails['password'])) { echo $emaildetails['password']; } ?>" />&nbsp;<a href="#" rel="tooltip" title="<?php _e('Admin SMTP Email Password', 'appointzilla'); ?>"><i class="icon-question-sign"></i></a>
	</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

  <tr>
    <th colspan="2" scope="row">&nbsp;</th>
    <td>&nbsp;</td>
    <td>
	
<?php if($emailtype && $emaildetails ) { ?>
		<button name="savesettings" class="btn btn-primary" type="submit" id="savesettings"><i class="icon-pencil icon-white"></i> <?php _e('Update Settings', 'appointzilla'); ?></button>
		<?php } else { ?>
		<button name="savesettings" class="btn btn-primary" type="submit" id="savesettings"><i class="icon-ok icon-white"></i> <?php _e('Save Settings', 'appointzilla'); ?></button>
		<?php } ?>
		<a href="?page=notificationsettings" class="btn btn-primary"><i class="icon-remove icon-white"></i> <?php _e('Cancel', 'appointzilla'); ?></a>
		</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
</form>




<style type="text/css">
.error{  color:#FF0000; 
}
</style>
<!--validation js lib-->
<script src="<?php echo plugins_url('/js/jquery.min.js', __FILE__); ?>" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	
	//---onload if check enable---
	var emailtype = $('#emailtype').val();
	if($('#enable').is(':checked'))
	{	
		$('#emailtype').attr("disabled", false);	//enable
		if(emailtype == 'wpmail')
		{
			$('#smtpdetails1').hide();
			$('#smtpdetails2').hide();
			$('#smtpdetails3').hide();
			$('#smtpdetails4').hide();
			$('#smtpdetails5').hide();
			
			$('#phpmaildetails1').hide();
			$('#phpmaildetails2').hide();
				
			$('#wpmaildetails1').show();
			$('#wpmaildetails2').show();
		}

		if(emailtype == 'phpmail')
		{
			$('#smtpdetails1').hide();
			$('#smtpdetails2').hide();
			$('#smtpdetails3').hide();
			$('#smtpdetails4').hide();
			$('#smtpdetails5').hide();
			
			$('#phpmaildetails1').show();
			$('#phpmaildetails2').show();
				
			$('#wpmaildetails1').hide();
			$('#wpmaildetails2').hide();
		}
		if(emailtype == 'smtp')
		{
			$('#smtpdetails1').show();
			$('#smtpdetails2').show();
			$('#smtpdetails3').show();
			$('#smtpdetails4').show();
			$('#smtpdetails5').show();
			
			$('#phpmaildetails1').hide();
			$('#phpmaildetails2').hide();
				
			$('#wpmaildetails1').hide();
			$('#wpmaildetails2').hide();
		}
	}
	else
	{
		$('#emailtype').attr("disabled", true);
	}
	
	
	
	
	<!---on-click--->
	$('#enable').click(function(){
	
		$(".error").hide();
		
		if ($(this).is(':checked'))
		{
			$('#emailtype').attr("disabled", false);
		}
		else
		{
			$('#emailtype').attr("disabled", true);
		}
	});
	
	<!---onchange email type--->
	$('#emailtype').change(function(){
		var emailtype = $('#emailtype').val();
		if($('#enable').is(':checked') && emailtype)
		{	
			if(emailtype=='wpmail')
			{
				$('#smtpdetails1').hide();
				$('#smtpdetails2').hide();
				$('#smtpdetails3').hide();
				$('#smtpdetails4').hide();
				$('#smtpdetails5').hide();
				
				$('#phpmaildetails1').hide();
				$('#phpmaildetails2').hide();
					
				$('#wpmaildetails1').show();
				$('#wpmaildetails2').show();
			}

			if(emailtype == 'phpmail')
			{
				$('#smtpdetails1').hide();
				$('#smtpdetails2').hide();
				$('#smtpdetails3').hide();
				$('#smtpdetails4').hide();
				$('#smtpdetails5').hide();
				
				$('#phpmaildetails1').show();
				$('#phpmaildetails2').show();
					
				$('#wpmaildetails1').hide();
				$('#wpmaildetails2').hide();
			}
			if(emailtype == 'smtp')
			{
				$('#smtpdetails1').show();
				$('#smtpdetails2').show();
				$('#smtpdetails3').show();
				$('#smtpdetails4').show();
				$('#smtpdetails5').show();
				
				$('#phpmaildetails1').hide();
				$('#phpmaildetails2').hide();
					
				$('#wpmaildetails1').hide();
				$('#wpmaildetails2').hide();
			}
		}
	});
		$('#savesettings').click(function(){
	$(".error").hide();
			
		//enable
		if (jQuery('#enable').is(':checked'))
		{
			var emailtype = jQuery('#emailtype').val();
			if(emailtype == 0)
			{
				$("#emailtype").after('<span class="error">&nbsp;<br><strong><?php _e('Select email type' ,'appointzilla'); ?></strong></span>');
				return false;
			}
			
			//wp-email
			if(emailtype == 'wpmail')
			{
				var wpemail = jQuery('#wpemail').val();
				if(wpemail == '')
				{
					$("#wpemail").after('<span class="error">&nbsp;<br><strong><?php _e('Enter wp email' ,'appointzilla'); ?></strong></span>');
					return false;
				}
				else
				{
					var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if(regex.test(wpemail) == false )
					{ 	
						jQuery("#wpemail").after('<span class="error">&nbsp;<br><strong><?php _e('Invalid email.' ,'appointzilla'); ?></strong></span>');
						return false; 
					}
				}
			}
			
			//php-email
			if(emailtype == 'phpmail')
			{
				var phpemail = jQuery('#phpemail').val();
				if(phpemail == '')
				{
					$("#phpemail").after('<span class="error">&nbsp;<br><strong><?php _e('Enter php email' ,'appointzilla'); ?></strong></span>');
					return false;
				}
				else
				{
					var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if(regex.test(phpemail) == false )
					{ 	
						jQuery("#phpemail").after('<span class="error">&nbsp;<br><strong><?php _e('Invalid email.' ,'appointzilla'); ?></strong></span>');
						return false; 
					}
				}
			}
			
			//smtp
			if(emailtype == 'smtp')
			{
				var hostname = jQuery('#hostname').val();
				if(hostname == '')
				{
					$("#hostname").after('<span class="error">&nbsp;<br><strong><?php _e('Enter host name' ,'appointzilla'); ?></strong></span>');
					return false;
				}
				
				var portno = jQuery('#portno').val();
				if(portno == '')
				{
					$("#portno").after('<span class="error">&nbsp;<br><strong><?php _e('Enter port number' ,'appointzilla'); ?></strong></span>');
					return false;
				}
				var portno = isNaN(portno);
				if(portno == true) 
				{ 	
					jQuery("#portno").after('<span class="error">&nbsp;<br><strong><?php _e('Invalid port number.' ,'appointzilla'); ?></strong></span>');
					return false; 
				}
				
				var smtpemail = jQuery('#smtpemail').val();
				if(smtpemail == '')
				{
					$("#smtpemail").after('<span class="error">&nbsp;<br><strong><?php _e('Enter email' ,'appointzilla'); ?></strong></span>');
					return false;
				}
				else
				{
					var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
					if(regex.test(smtpemail) == false )
					{ 	
						jQuery("#smtpemail").after('<span class="error">&nbsp;<br><strong><?php _e('Invalid email.' ,'appointzilla'); ?></strong></span>');
						return false; 
					}
				}
				
				var password = jQuery('#password').val();
				if(password == '')
				{
					$("#password").after('<span class="error">&nbsp;<br><strong><?php _e('Enter password' ,'appointzilla'); ?></strong></span>');
					return false;
				}
			}
		}
	});
});
</script>
</div>